.. _operations:

Operations
==========

Management tasks and other operations can be performed within the shell
of deployed web services.

TODO: The service needs to be hosted on a reliable resource
(out of the OC servers, that are contninuously destroyed and recreated)

The following instructions are intended for a containerized deploy.

.. _dcenv:
Environment prerequisites
-------------------------
To work with ``docker-compose`` with the correct environment variables set,
you need to create a ``config/.dcenv`` file, assigning values to the
following variables:

.. code-block:: bash

    export POSTGRES_DB=[a db name]
    export POSTGRES_USER=[a user]
    export POSTGRES_PASS=[a password]
    export SECRET_KEY="[a generated secret key]"
    export ALLOWED_HOSTS=[the domains, comma-separated, or *]


Then the env variable can be given values with:

.. code-block:: bash

    eval $(cat config/.dcenv)

You can use the shortcut ``dcenv_set``, if the ``config/dcenv_functions``
file has been sourced.

.. _setup_operations:
Setup
-----
Once the stack has been deployed, then the machine needs to be setup.

.. code-block:: bash

    dc run --rm tasks python manage.py migrate
    dc run --rm tasks python manage.py createsuperuser --username admin --email guglielmo@depp.it
    dc run --rm tasks python manage.py collectstatic --noinput

Data import
^^^^^^^^^^^

Territori data must be copied on the docker container, then loaded in the DB.

.. code-block:: bash

    docker cp resources/fixtures/territori.json octerritori_web_1:/app/data/


.. code-block:: bash

    dc run --rm tasks python manage.py loaddata data/territori.json -v3


Services operations
-------------------

To restart all services, after a change in ``docker-compose.yml``:

.. code-block:: bash

    dc up -d

.. note:: Images need to be rebuilt whenever there are changes
    in the source code.

.. code-block:: bash

    dc up --build

To enter a shell in the container of the web service for debugging purposes:

.. code-block:: bash

    dc exec web bash

This can be done also to enter the other containers, but it shouldn't
be necessary.

To stop all services and remove all containers:

.. code-block:: bash

    dc down


Database Migrations
-------------------

Migrations must be executed on the local machine.
Then the web image must be rebuilt
Then they need to be run on the docker container.

The following instructions can be followed to generate simplified
topojson boundaries used in the OpenCoesione2 web application.

The procedure will work on the deployed, containerized stack and
generates the ``.topo.json`` files in the ``octerritori_web_data``
shared stack volume.



Territory merge
---------------

Territories can be merged using the `comuni_merge` management task.

.. code-block:: bash

    python manage.py comuni_merge --filename resources/data/fusioni_comuni_20180630.xlsx -v2


The complete geojson can be generated from the DB with:

.. code-block:: bash

    python manage.py get_geojson -v2


After that all the topojson files need to be regenerated.
see :ref:`maps`


Transfer files on OsX
^^^^^^^^^^^^^^^^^^^^^

Generated files need to be moved into the
``project/static/topojson`` directory of the oc2 project.
Then on a push, they will be used in the new automatically generated image.

Backup
------

Il dump del database viene generato e compressso, poi inviato su S3, per uno storage a lungo termine.
Solo le ultime due versioni del dumpo sono mantenute in locale.
.. code-block::

    pg_dump -Upostgres oc_territori > oc_territori.dump.YYYYMMDD.sql
    gzip oc_territori.dump.YYYYMMDD.sql
    aws --profile s3 s3 copy oc_territori.dump.YYYYMMDD.sql.gz s3://open_coesione/oc_territori