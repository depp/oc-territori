Introduction
============

The OpenCoesione Territori service, allows the management of the
Territori items for the `OpenCoesione` project.

The service allows a user to download a json of all locations'centroids from
a single endpoint, for integration purposes.

.. code-block:: bash

    wget http://staging.opencoesione.it:8001/centroids -o centroids.json

A mangement task allows to export all geographic boundaries in geo-json
format, so that the production of the topojson needed for the main application
maps can be executed. See :ref:`operations`.


