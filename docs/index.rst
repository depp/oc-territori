.. Openpolis DataManager documentation master file, created by
   sphinx-quickstart.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to OpenCoesione Territori Managent!
===========================================

Contents:

.. toctree::
   :maxdepth: 2

   intro
   install
   deploy
   operations
   maps


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
