.. _deploy:

Deploy
========

Intro
-----
The application stack is vertically self-contained on a single host.

The stack is defined in ``docker-compose.yml`` file.
It uses:

  - a django app (web)
  - a no-service django container for tasks
  - a frontend web server (nginx)
  - a storage RDBMS (postgres)

.. _local_deploy:

Local docker-compose deploy
---------------------------
``docker-compose`` can be used to test the stack locally.

Environment variables need to be defined:

.. code-block:: bash

    export POSTGRES_DB=[DB_NAME]
    export POSTGRES_USER=[DB_USER]
    export POSTGRES_PASS=[DB_PASSWORD]
    export SECRET_KEY=[SECRET_KEY]
    export ALLOWED_HOSTS=*

The web image must be built:

.. code-block:: bash

    docker build -t registry.gitlab.depp.it/depp/oc-territori:latest .

To deploy:

.. code-block:: bash

    dc up -d web nginx postgre

    dc run --rm tasks python manage.py [TASK]
    ...

dc stands for ``docker-compose`` here, a proper installation should
automatically provide you the shortcut.


Postgis DB creation
^^^^^^^^^^^^^^^^^^^

The image used in the stack creates a dummy database schema. The
following operations are needed in order to regenerate an empty
schema, where migrations can be appllied:

.. code-block:: bash

    dc exec postgres dropdb -Upostgres oc_territori
    dc exec postgres createdb -Upostgres oc_territori


Postgresql data are **persisted** under a docker ``volume``, and
**won't be lost** after a shut down of all services.


Setup
^^^^^

See :ref:`setup_operations` for setting the application up.


.. _remote_deploy:

Remote deploy with docker-machine
---------------------------------

The stack can be deployed on a remotely created machine,
using ``docker-machine`` and ``docker-compose``.

The web application docker image must be first pushed to the gitlab registry:

.. code-block:: bash

    docker login registry.gitlab.depp.it
    docker push registry.gitlab.depp.it/depp/oc-territori:latest

The remote machine must already have been created and provisioned:

.. code-block:: bash

    # set environment variable
    export TOKEN=[DIGITAL_OCEAN_TOKEN]

    # generazione di una docker-machine su digital ocean
    docker-machine create --driver digitalocean \
      --digitalocean-access-token $TOKEN \
      --digitalocean-image ubuntu-16-04-x64 \
      --digitalocean-region fra1 \
      --digitalocean-size 16gb \
      --digitalocean-ssh-key-fingerprint 99:c6:71:66:39:df:bc:15:ed:2e:7c:61:96:28:ca:ec \
    oc2-staging

A connection to the remote machine must be made, so that
``docker-machine`` will operate on that server, instead of locally:

.. code-block:: bash

    docker env oc2-staging
    eval $(docker env oc2-staging)

Finally, the stack can be created, after having set all environment variables
required by ``docker-compose``.

.. code-block:: bash

    export POSTGRES_DB=[DB_NAME]
    export POSTGRES_USER=[DB_USER]
    export POSTGRES_PASS=[DB_PASSWORD]
    export ALLOWED_HOSTS=*
    dc up -d
