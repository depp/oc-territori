.. _maps:

Maps
====

This procedure describes how to re-generate the topojson files needed by
the d3-powered map of the OpenCoesione application.

Commands must be run within the projects' virtual env, inside the development machine (up to now).

`mapshaper` command is needed, and it requires node.js and npm: it can be installed on osx with:

.. code-block:: bash

    brew install nodejs npm
    npm install -g mapshaper


Adjusting merge operations
--------------------------
Locations modifications (merge), performed on production or staging DB, must be repeated locally.
This can be both performed manually, or by dedicated management tasks, launched in the virtual env.
The file needed by this management task is an xlsx file.

.. code-block:: bash

    python manage.py comuni_merge --file-name resources/data/variazioni_comuni_20200404.xlsx --verbosity=2

Generating topojson limits
--------------------------

``oc_comuni.geo.json``, contains all comuni geojson
limits, as extracted from the ``oc-territori`` service, through the
``get_geojson`` management tasks.

It is used as the seed to generate the topojson files needed by the d3-based
map (see the oc2 manual, section maps).

.. code-block:: bash

        python manage.py get_geojson -v2 \
        --file-name resources/data/oc_comuni.geo.json


The simplified topojson, containing all three layers is needed:

.. code-block:: bash

    # translation into simplified topojson
    mapshaper\
        -i resources/data/oc_comuni.geo.json -clean \
            encoding=utf8 \
        -simplify 5% weighted \
        -o resources/data/oc_comuni.simplified.topo.json bbox format=topojson

    # aggregated layers generation
    mapshaper\
        -i resources/data/oc_comuni.simplified.topo.json \
        -rename-layers comuni \
        -dissolve cod_pro + name=province \
        -rename-fields cod=cod_pro \
        -target 1 \
        -dissolve cod_reg + name=regioni \
        -rename-fields cod=cod_reg \
        -target 1  \
        -dissolve cod_area + name=aree \
        -rename-fields cod=cod_area \
        -o resources/data/limits.topo.json bbox target='*' format=topojson \
        -info


A ``limits_it.topo.json`` file containing only ``regioni``, ``province`` and ``aree`` layers,
can then be produced through:

.. code-block:: bash

    mapshaper\
        -i resources/data/limits.topo.json \
        -drop target=comuni  \
        -o resources/data/json/limits_it.topo.json bbox format=topojson  target='*'


To produce the 20 regional layers:

.. code-block:: bash

    for REG in `seq 1 20`
    do
    mapshaper\
        -i resources/data/oc_comuni.simplified.topo.json \
        -filter cod_reg==$REG \
        -dissolve cod_pro + \
        -target 1 \
        -dissolve cod_area + \
        -rename-layers comuni,province,aree \
        -filter-fields cod_com target=comuni \
        -rename-fields cod=cod_com target=comuni \
        -rename-fields cod=cod_pro target=province \
        -rename-fields cod=cod_area target=aree \
        -o resources/data/json/limits_R${REG}.topo.json bbox format=topojson target=comuni,province,aree

    done

To generate the provinces layers:

.. code-block:: bash

    for PROV in `seq 1 111`
    do
    mapshaper\
        -i resources/data/oc_comuni.geo.json \
        -simplify 50% weighted \
        -filter cod_pro==$PROV \
        -rename-layers comuni target=1 \
        -filter-fields cod_com target=comuni \
        -rename-fields cod=cod_com target=comuni \
        -o resources/data/json/limits_P${PROV}.topo.json bbox format=topojson target=comuni
    done


To generate the areas layers:

.. code-block:: bash

    for AREA in ABR_AI1 ABR_AI2 ABR_AI3 ABR_AI4 ABR_AI5 BAS_AI1 BAS_AI2 BAS_AI3 BAS_AI4 CAL_AI1 CAL_AI1 CAL_AI2 CAL_AI3 CAL_AI4 \
        CAM_AI1 CAM_AI2 CAM_AI3 CAM_AI3 CAM_AI4 EMR_AI1 EMR_AI1 EMR_AI2 EMR_AI2 EMR_AI3 EMR_AI3 EMR_AI4 EMR_AI4 \
        FVG_AI1 FVG_AI2 FVG_AI2 FVG_AI3 LAZ_AI1 LAZ_AI2 LAZ_AI3 LAZ_AI4 LIG_AI1 LIG_AI2 LIG_AI3 LIG_AI4 \
        LOM_AI1 LOM_AI2 LOM_AI3 LOM_AI4 MAR_AI1 MAR_AI2 MAR_AI3 MOL_AI1 MOL_AI2 MOL_AI3 MOL_AI4 PAT_AI1 PAT_AI1 PAT_AI2 \
        PIE_AI1 PIE_AI2 PIE_AI2 PIE_AI3 PIE_AI4 PIE_AI4 PUG_AI1 PUG_AI1 PUG_AI2 PUG_AI2 PUG_AI3 PUG_AI3 PUG_AI4 PUG_AI4 \
        SAR_AI1 SAR_AI2 SIC_AI1 SIC_AI1 SIC_AI2 SIC_AI3 SIC_AI4 SIC_AI5 TOS_AI1 TOS_AI1 TOS_AI2 TOS_AI2 TOS_AI3 TOS_AI3 \
        UMB_AI1 UMB_AI2 UMB_AI2 UMB_AI3 VDA_AI1 VDA_AI2 VDA_AI2 VEN_AI1 VEN_AI2 VEN_AI3 VEN_AI4
    do
        echo $AREA

        mapshaper \
            -i resources/data/oc_comuni.geo.json \
            -simplify 50% weighted \
            -filter cod_area==="'$AREA'" \
            -rename-layers comuni \
            -rename-fields cod=cod_com \
            -filter-fields cod,cod_rip_area \
            -dissolve fields=cod,cod_rip_area + \
            -rename-layers comuni_ap target=2 \
            -filter cod_rip_area=="'AP'" target=2 \
            -require js/logic.js -run 'conditionalFieldsFilter(target)' target=2 \
            -target 1 \
            -dissolve fields=cod,cod_rip_area + \
            -rename-layers comuni_as target=3 \
            -filter cod_rip_area=="'AS'" target=3 \
            -require js/logic.js -run 'conditionalFieldsFilter(target)' target=3 \
            -filter-fields cod target=1 \
            -o resources/data/json/limits_${AREA}.topo.json bbox format=topojson target='*'
    done


At this point the ``limit_*.json`` files are available under the
``octerritori_web_data`` volume, and can be copied under stati


Centroids generation
--------------------

Centroids are needed in the main application and can be generated by running the service and querying the `/centroids`
endpoint.

.. code-block:: bash

    curl http://localhost:8000/centroids > resources/data/json/centroids.json
