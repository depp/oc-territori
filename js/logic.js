module.exports = {
	conditionalFieldsFilter: function(target, log=0) {
		if (log) {
			console.log(target.layer);
		}
		if (target.feature_count > 0) {
			return `-filter-fields cod target=`+ target.layer.name;
		}
	}
};