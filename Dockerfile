FROM python:3.6-slim

# update, upgrade and install useful tools and aliases
RUN apt-get -y update \
    && apt-get install -y --no-install-recommends apt-utils \
    && apt-get -y upgrade \
    && apt-get install -y --no-install-recommends git-core tmux vim locales libspatialite5 \
    && apt-get install -y --no-install-recommends gcc python-dev \
    && apt-get install -y --no-install-recommends binutils libproj-dev gdal-bin
#    && rm -rf /var/lib/apt/lists/*
RUN echo 'alias ll="ls -l"' >> ~/.bashrc && echo 'alias la="ls -la"' >> ~/.bashrc

# add it locale
COPY locales.txt /etc/locale.gen
RUN locale-gen

# upgrade pip
RUN pip3 install --upgrade pip

# initialize app
RUN mkdir -p /app
WORKDIR /app

# install python requirements
COPY requirements.txt /app
RUN pip3 install -r requirements.txt

# production needs gunicorn
RUN pip3 install gunicorn==19.7.1

# remove gcc and builkd dependencies to keep image small
RUN apt-get purge -y --auto-remove gcc python-dev

# copy app
COPY . /app/

RUN rm /app/locales.txt
