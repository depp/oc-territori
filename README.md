# OpenCoesione Territori Manager

Lightweight django application that handles the OpenCoesione Territori
within a Postgis DB.

Management tasks can be launched here to perform operations
on locations, then the data can be exported in various format
to be transfered into OC2.

Operations must be performed on the local, development machine.

This repository is precious, as there is no running instance.
The source code is on gitlab: https://gitlab.depp.it/depp/oc-territori.

Databases dump are kept on AWS S3, under open_coesione/oc-territori.
Backup instructions are in teh docs, at the `operations` section.

