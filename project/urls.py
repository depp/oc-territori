from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin

from project.territori.views import centroids
from .views import index, ping

admin.autodiscover()


urlpatterns = [
    url(r'^admin/', admin.site.urls),
]

urlpatterns += [
    url(r'^$', index),
    url(r'^ping$', ping),
    url(r'^centroids$', centroids)
]

if settings.DEBUG_TOOLBAR:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
