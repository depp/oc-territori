# -*- coding: utf-8 -*-
import logging
from django.core.management.base import BaseCommand
from django.core.serializers import serialize
from project.territori.models import Territorio


class Command(BaseCommand):
    help = 'Extract geojson from Territori model in geo-django'
    logger = logging.getLogger(__name__)

    def add_arguments(self, parser):
        # Named (optional) arguments
        parser.add_argument(
            '--file-name',
            dest='file_name',
            default='oc_comuni.geo.json',
            help='Name of the geojson file'
        )

    def handle(self, *args, **options):
        verbosity = options['verbosity']
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)

        filename = options['file_name']

        self.logger.info("Serializing comuni ...")
        comuni_json = serialize(
            'geojson', Territorio.objects.comuni(),
            geometry_field='geom',
            fields=('cod_reg', 'cod_pro', 'cod_com', 'cod_area', 'cod_rip_area', 'denominazione')
        )

        self.logger.info("Writing {0} ...".format(filename))
        with open(filename, 'w') as fp:
            fp.write(comuni_json)

        self.logger.info("Finished.")
