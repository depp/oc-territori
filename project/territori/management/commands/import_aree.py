# -*- coding: utf-8 -*-
import datetime
import logging
import pandas as pd

from django.core.management.base import BaseCommand
from django.db import transaction

from ...models import Territorio

class Command(BaseCommand):
    help = 'Import aree for given comuni, from comuni_aree_YYYYMMDD.csv file'
    logger = logging.getLogger(__name__)

    def add_arguments(self, parser):
        parser.add_argument(
            '--file-name',
            dest='file_name',
            default='resources/data/comuni_aree_20201231.csv',
            help='Name of the CSV file'
        )

    def handle(self, *args, **options):
        verbosity = options['verbosity']
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)
        filename = options['file_name']

        self.logger.info('Inizio import dal file {}.'.format(filename))

        start_time = datetime.datetime.now()

        df = pd.read_csv(filename, delimiter=";")
        n_rows = len(df)
        for n, (idx, row) in enumerate(df.iterrows(), start=1):
            if n % 100 == 0:
                self.logger.info(f"{n}/{n_rows}")
            try:
                t = Territorio.objects.get(
                    territorio='C',
                    cod_com=row['COD_PROVINCIA'] * 1000 + row['COD_COMUNE'],
                )
                t.cod_area = row['COD_AREA']
                t.cod_rip_area = row['COD_PARTIZIONE']
                t.save()
            except Territorio.DoesNotExist:
                self.logger.error(f"Couldn't find territorio {row['COD_COMUNE']}, {row['COD_PROVINCIA']}. Skipping.")
            except Exception as e:
                self.logger.error(f"Couldn't update territorio {e}. Skipping.")

            area, created = Territorio.objects.get_or_create(
                cod_area=row['COD_AREA'],
                territorio='AI',
                defaults={
                    'denominazione': row['DENOM_AREA']
                }
            )
            if created:
                self.logger.info(f"Area {area.denominazione}, type: {area.territorio} created (code: {area.cod_area}).")

        duration = datetime.datetime.now() - start_time
        seconds = round(duration.total_seconds())

        self.logger.info(
            f"Fine. Tempo di esecuzione: "
            f"{int(seconds // 3600):02d}:{int((seconds % 3600) // 60):02d}:{int(seconds % 60):02d}"
            "."
        )
