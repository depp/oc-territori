# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand

from ...models import Territorio

class Command(BaseCommand):
    help = 'List cod_com for all comuni.'

    def handle(self, *args, **options):

        for t in Territorio.objects.filter(territorio='C').order_by('cod_com').values_list('cod_com', flat=True):
            print(t)

