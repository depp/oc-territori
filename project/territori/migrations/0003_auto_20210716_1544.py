# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2021-07-16 13:44
from __future__ import unicode_literals

from django.db import migrations, models
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('territori', '0002_auto_20180208_2040'),
    ]

    operations = [
        migrations.AddField(
            model_name='territorio',
            name='cod_area',
            field=models.CharField(blank=True, db_index=True, max_length=7, null=True),
        ),
        migrations.AlterField(
            model_name='territorio',
            name='slug',
            field=django_extensions.db.fields.AutoSlugField(blank=True, editable=False, max_length=256, populate_from='nome', unique=True),
        ),
        migrations.AlterField(
            model_name='territorio',
            name='territorio',
            field=models.CharField(choices=[('C', 'Comune'), ('P', 'Provincia'), ('R', 'Regione'), ('N', 'Nazionale'), ('E', 'Estero')], db_index=True, max_length=2),
        ),
    ]
