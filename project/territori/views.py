# coding: utf-8
import json

from django.contrib.gis.db.models.functions import AsGeoJSON, Centroid
from django.http import JsonResponse

from project.territori.models import Territorio


def centroids(request):
    regioni = Territorio.objects.exclude(geom__isnull=True).annotate(
        centroid=AsGeoJSON(Centroid('geom'))
    )
    results = []
    for t in regioni:
        centroid = json.loads(t.centroid)
        results.append({
            'cod_reg': t.cod_reg,
            'cod_pro': t.cod_pro,
            'cod_com': t.cod_com,
            'centroid': {
                'lat': centroid['coordinates'][1],
                'lon': centroid['coordinates'][0]
            }
        })

    return JsonResponse({
        'status': 'OK',
        'results': results
    })




