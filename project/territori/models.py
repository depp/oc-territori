# -*- coding: utf-8 -*-
import struct
from django.contrib.gis.db import models
from django.utils.functional import cached_property
from django_extensions.db.fields import AutoSlugField


class TerritorioManager(models.GeoManager):
    def nazione(self):
        return self.get_queryset().get(territorio=self.model.N)

    def regioni(self, with_nation=False):
        codes = [self.model.R]
        if with_nation:
            codes.append(self.model.N)
            codes.append(self.model.E)
        return self.get_queryset().filter(territorio__in=codes)

    def provincie(self):
        return self.get_queryset().filter(territorio=self.model.P)

    def comuni(self):
        return self.get_queryset().filter(territorio=self.model.C)

    @cached_property
    def regioni_by_cod(self):
        return {o.cod_reg: o for o in self.regioni()}

    @cached_property
    def provincie_by_cod(self):
        return {o.cod_prov: o for o in self.provincie()}

    def get_from_istat_code(self, istat_code):
        """
        Get single record from Territorio, starting from ISTAT code
        ISTAT code has the form RRRPPPCCC, where
         - RRR is the regional code, zero padded
         - PPP is the provincial code, zero padded
         - CCC is the municipal code, zero padded

        If a record in Territorio is not found, then the ObjectDoesNotExist
        exception is thrown
        """
        if istat_code is None:
            return None

        if len(istat_code) != 9:
            return None

        (cod_reg, cod_pro, cod_com) = struct.unpack('3s3s3s', istat_code)
        return self.get_queryset().get(
            cod_reg=int(cod_reg),
            cod_pro=int(cod_pro),
            cod_com=str(int(cod_pro))+cod_com
        )


class Territorio(models.Model):
    N = 'N'
    C = 'C'
    P = 'P'
    R = 'R'
    E = 'E'
    AI = 'AI'
    TERRITORIO = (
        (C, 'Comune'),
        (P, 'Provincia'),
        (R, 'Regione'),
        (N, 'Nazionale'),
        (E, 'Estero'),
        (AI, 'Area Interna'),
    )

    AP = 'AP'
    AS = 'AS'
    COD_RIP_AREA = (
        (AP, 'Area progetto'),
        (AS, 'Area strategica')
    )

    cod_reg = models.IntegerField(
        default=0, blank=True, null=True, db_index=True
    )
    cod_pro = models.IntegerField(
        default=0, blank=True, null=True, db_index=True
    )
    cod_com = models.IntegerField(
        default=0, blank=True, null=True, db_index=True
    )
    cod_area = models.CharField(
        max_length=7, blank=True, null=True, db_index=True
    )
    cod_rip_area = models.CharField(
        max_length=2, blank=True, null=True, choices=COD_RIP_AREA
    )
    denominazione = models.CharField(
        max_length=128, db_index=True
    )
    denominazione_ted = models.CharField(
        max_length=128, blank=True, null=True, db_index=True
    )
    slug = AutoSlugField(
        populate_from='nome',
        max_length=256, unique=True, db_index=True
    )
    territorio = models.CharField(
        max_length=2, choices=TERRITORIO, db_index=True
    )
    geom = models.MultiPolygonField(
        srid=4326, null=True, blank=True
    )
    popolazione_totale = models.IntegerField(null=True, blank=True)
    popolazione_maschile = models.IntegerField(null=True, blank=True)
    popolazione_femminile = models.IntegerField(null=True, blank=True)

    objects = TerritorioManager()

    @property
    def nome(self):
        if self.denominazione_ted:
            return u'{} - {}'.format(self.denominazione, self.denominazione_ted)
        else:
            return u'{}'.format(self.denominazione)

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = 'Località'
        verbose_name_plural = 'Località'
        ordering = ['denominazione']
