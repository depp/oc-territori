# coding: utf-8

from collections import OrderedDict

from django.http import JsonResponse
from django.views.decorators.http import require_http_methods


def ping(request):
    return JsonResponse(
        {'message': 'All is well!'}
    )


@require_http_methods(["GET", "HEAD"])
def index(request):
    return JsonResponse(OrderedDict([
        ('views list', OrderedDict([
            ('ping', "/ping"),
            ('centroids', "/centroids")
        ])),
    ]))


